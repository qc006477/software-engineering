Module Code:  CS1SE20
Assignment report Title:    Requirements Report
Student Number: 30021313
Date: 26/11/21 
Actual hours spent for the assignment: 3

#.2 What key points do you need to clarify?

Introduction

This answer discusses what points should be identified and explored before beginning the design phase of the game.
The game is to be designed to be fun, entertaining and enticing to university students, whilst including set readings about history of the Atlantic for the course(s); the main idea is that the students will (i) want to play the game and (ii) will learn the set readings whilst playing it.
Hence, the following the key points need to be clarified before the development of the game can continue.

Main Body

1. What the game will run off of, and whether a game engine will be necessary. This is very important, since using a game engine requires a different set of skills than creating a game from self-written code alone.
This will determine who will be able to work on the development of the program, and may also influence what language is used, e.g. if the game engine is limited to a specific language or set of languages.
Game engines make development of more advanced games such as games with 3D environments significantly quicker and easier to make. They exist as a basic backbone for the environment(s), and usually have tools and an interface that assist in creating the games.
However, they can often be costly and complicated, requiring experts to take full advantage of the software, and this may be too much hassle if the game is simple enough to be created with self-written code in a reasonable amount of time.

2. The general specifications of the machines the game will most likely be run off of. This allows the game to be tailored for use of those computers based on their performance limitations. This ensures the game can run comfortably on the computers, so that it is not a frustrating or bad experience to play, as this will deter the students rather than attract them.

3. How is the information about the set readings going to be delivered and how are the students going to learn it? It should be clear how the students will learn the information, and the game should force them to use and work with the set readings so that they remember them.
This could involve using them to be parts of puzzles so that the students actually have to think about and process the given information in order to progress in the game.

4. How the game will basically function. The basic idea of the game, whether it’s a 3D first-person game or a 2D platformer, should be established. This is so that there is a starting point for when the idea for the game will be added to and refined.

5. What the point of the game is. It should be very clear what the game is supposed to do for the students, so that the end result is focused on it. 






Assignment evaluation

Key points include:
•	The information about the performance limits and base requirements in terms of language and software for the game development.
•	What the game should do for the students.
•	How is the key set reading information going to be delivered to the students in a that will help them remember.

