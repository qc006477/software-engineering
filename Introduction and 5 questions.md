Title page (the following are compulsory)

Module Code:  CS1SE20

Assignment report Title: Requirements Report

Student Number (e.g. 25098635): 30021392, 30021421, 30006477, 30020762, 30021313

Date (when the work completed): 25/11/2021

Actual hrs spent for the assignment: 30 hours



Assignment evaluation (3 key points): 
Key points include:
•	The information about the performance limits and base requirements in terms of language and software for the game development.
•	What the game should do for the students.
•	How is the key set reading information going to be delivered to the students in a way that will help them remember?
Introduction
A customer wants an instructional "video" game to help motivate their university students to complete the required readings. A puzzle game and a first-person adventure have been offered as possibilities. It's uncertain if the game should incorporate some of the information from the readings or feature riddles that must be solved by reading them. There was no indication of whether it should be single-player or multiplayer – competitive or collaborative – however a high-score table was mentioned.
 
To do this we will be using the agile project management style to complete the task for the stakeholder quickly and efficiently. This style lets us integrate our work as we develop, and make sure that we stay on track and fulfil the stakeholders' needs efficiently.
 
#.1 Who (stakeholders) should you be asking about the requirements?
Hint: there may be more than one or two “classes” of people in the answer
 
There may be two types of people that we will be asking about the requirements for the game, the customer and people using the app. Regarding the customer, we will keep them updated throughout the development process, this way if we are not hitting the requirements, or the customer wants something changed, we can do so easily. This way we won't get a completed project and it will not be what the customer wanted in the first place.
Another stakeholder I will need to ask is the teachers who teach the content to the students in lectures. we will need to make sure that the content in the game is relevant in the first place and is going to be useful to students. We will keep in close contact with them throughout the development to again make sure we're keeping on tracks.
The students playing the game are also important stakeholders to ask about requirements. We will ask them what would make the game enjoyable and keep them playing it, so it doesn't feel like work, but an actual game they're willing to play. We will also ask them what parts of the work they're struggling with/not interacting with. This way we can home in on the content to what they really need help with/find uninteresting. We can also figure out from the students if they would rather play a puzzle game, or a 1st person adventure, and whether they would want it to be multiplayer.
Finally, another stakeholder could be us (the people making the game), specifically the people programming the game. They will be working on different aspects of the game each and will need to make sure that all the requirements are being done and who's doing it.
 
#.2 What key points do you need to clarify?
Hint: this could be a very long list – shortlist the top ten in terms of importance
 
Purpose of the game- The team's attention should be on the project's purpose, which should be clarified. This should be an instructional video game aimed at university students to encourage them to complete their assigned readings. This goal is crucial to projects since it assists in directing actions needed within the correct standards; yet it also assists the team in ignoring actions that are unnecessarily detrimental to the project. 
 
Long term goals- Successful video games make you feel compelled to keep playing... or you'll lose interest. While many players enjoy open-world games in which they can spend days talking to locals or farming the land, it's critical that these short-term objectives lead to longer-term objectives. Otherwise, you'll quickly lose interest. With each triumph, a player should feel as if they have accomplished something. The best video games strike a delicate balance between allowing you to move at your own pace while also keeping your attention on the bigger difficulties that lie ahead. 
 
Replaying the game- It's also critical for games to have tempting reward systems that make your efforts worthwhile. New levels, characters, weapons, secrets, and achievements should all be available. A good game provides you a cause to keep playing whether you're playing it for the first time or the hundredth. 
 
Characters- It's critical to have some form of emotional connection to the characters in a game. Simple can be just as effective as complex when it comes to telling a compelling tale. Of course, in an RPG or adventure game, you want to meet a diverse group of interesting people. Even if you never hear your character speak, successful games have a way of making you care for them. 
 
Controls- The most popular games are simply enjoyable to play. Controls, when done correctly, make a game simple to learn but tough to master. Without having to think about it, you should be able to make natural, split-second decisions. Camera controls are one of the most common areas where games fall short. To get your character to accomplish what you want, you should be able to angle your vision just correctly. 
 
Game consistency- the most successful games can strike a balance between uniqueness and staying true to the model that fans of the genre and prequels enjoy. Without losing their sense of self, successful games reinvent themselves to bring something new to the table. 
 
Order of content- It is important to understand if there is a specific order of how and when the content is delivered. You would want the student to understand and gain knowledge in the correct way so they can implement this throughout the game. 
 
Creating an account- It is important for the learner to save their progress throughout the game. This could be done if they are able to create accounts and save their knowledge. 
 
Number of students- Is the game going to be single player or multiplayer, this is vital as it will affect the functionality of the game. It is important and should be clear for the student to understand whether the game will be group work or not. 
 
Casual or competitive- The student should understand if the game is either. If competitive, will there be levels and rewards? 
 
#.3 What constraints might there be?
Hint: technical, legal, and any others you can think of
 
Realistically there will be a few constraints when creating such a game that requires the collaboration of both having fun and trying to learn something educational. An obvious drawback that is quite impactful to this project is that it really depends on the customer’s learning style, and whether the game can compensate for the way they learn instead of acting as a source of distraction. Furthermore, there will be disabled people and there will be people who are not able to learn information online and need face to face communication in order to digest information which would struggle if they were to use something such as this. And as the insert we’ve been given says “ It is unclear whether the game should include some of the information in the readings, or whether it should include puzzles which need to be solved by reading them”, when choosing how to create a game it is often quite difficult to get straight results into how the learning “game” is able to directly link into what customers are trying to learn and achieve in terms of progressing their knowledge of the subject. So figuring out a balance of maintaining the game to be more towards education rather than being too “gamified” can also be seen as a hard external factor to control. The high score table, although could be seen as an encouraging way of promoting competition between customers and students to do better than each other, can also be seen as a negative reinforcement, since everyone is able to see the ranks people who are in the lower side of spectrum might be averted away from wanting to play the game since there score would be out in the public. Also making a game would require a lot of funds as the cost of creating a game on this level requires equipment costs, software costs etc. People tend to tackle this through making students pay or having a fee in order to download the game, but this can backfire causing people to not want to buy a game that would cost them money.
 
#.4 Which points need clarification first?
Hint: sometimes a requirement or constraint can be a “deal breaker” and make a project impossible in conjunction with the other requirements and constraints – if you find any of these, identify them clearly)
 
In the day and age of the 21st Century, technology and distractions are everywhere. Students are overwhelmed with technology within the educational system. It is impossible for the human species to not be diverted to all the psychological urges to view something interesting on a computer screen and therefore, one of the limitations within the requirements that need to be addressed is that students may well play the game however, it does not guarantee that the student will be motivated to read the work. Furthermore, not all video games are appealing for everyone therefore, some students may find the video game very boring, and it may encourage them to divert their attention to something less boring such as playing video games that they enjoy in their spare time instead of reading. Unfortunately, some students may get the ick from even spectating certain games that they do not enjoy in the slightest, this may potentially demotivate students to do the required reading. If this was to happen then it may be very difficult moving forward trying to motivate those students to read. 
 
Moreover, some students are not driven by video games and may find all video games boring therefore, it may possibly just not motivate those students at all. Fortunately, some students have found an escape from technology and that no longer excites them. If this does happen then it will be likely that the video game is very inconsistent and inefficient. These are one of the major problems that makes the video game idea that is intended to motivate students very difficult to create as the video game that is shared on all educational websites will only motivate and encourage some students but not all students therefore, it will not fully fix the problem it intends to fix.
 
Thank you for reading, Our Group


